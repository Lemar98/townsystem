package ru.lemar98.townsystem.town;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.database.TownData;
import ru.lemar98.townsystem.utils.ChatUtils;

import java.util.*;

public class Town {

    private JavaPlugin plugin;
    private TownData townData;
    private String name;
    private String uuid;
    private String mayor;
    private String assistant;
    private String board;
    private String registeredDate;
    private int lvl;
    private int nowExperience;
    private int requiredExp;
    private List<String> residents;

    public Town(String name, JavaPlugin plugin) {
        this.plugin = plugin;
        this.townData = new TownData(name, plugin);
        this.name = this.townData.getYaml().getString("townName").contains("&")
                ? this.townData.getYaml().getString(ChatColor.translateAlternateColorCodes('&', "townName"))
                : this.townData.getYaml().getString("townName");
        this.uuid = this.townData.getYaml().getString("townUuid");
        this.mayor = this.townData.getYaml().getString("Mayor");
        this.assistant = this.townData.getYaml().getString("assistant");
        this.board = this.townData.getYaml().getString("board");
        this.registeredDate = this.townData.getYaml().getString("registeredDate");
        this.lvl = this.townData.getYaml().getInt("lvl");
        this.nowExperience = this.townData.getYaml().getInt("nowExperience");
        this.requiredExp = this.townData.getYaml().getInt("requiredExperience");
        this.residents = this.townData.getYaml().getStringList("residents");
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
        townData.getYaml().set("board", this.board);
        townData.save();
    }

    public String getName() {
        return name;
    }

    public String getUuid() {
        return uuid;
    }

    public String getMayor() {
        return mayor;
    }

    public List<String> getResidents() {
        return residents;
    }

    public boolean isMayor(String uuid) {
        return mayor.equals(uuid);
    }

    public Collection<Player> getOnlinePlayers() {
        Collection<Player> onlines = new ArrayList<>();
        for(String uuid : getResidents()) {
            if(Bukkit.getPlayer(UUID.fromString(uuid)) != null) {
                onlines.add(Bukkit.getPlayer(UUID.fromString(uuid)));
            }
        }
        return onlines;
    }

    public void setAssistant(String uuid) {
        assistant = uuid;
        townData.getYaml().set("assistant", uuid);
        townData.save();
    }

    public void setMayor(String uuid) {
        mayor = uuid;
        townData.getYaml().set("Mayor", uuid);
        townData.save();
    }

    public String getAssistant() {
        return assistant != "!$none$!" ? assistant : "нет";
    }

    public boolean hasAssistant() {
        return assistant == "!$none$!" ? false : true;
    }

    public boolean isAssistant(String uuid) {
        return assistant.equals(uuid);
    }

    public void addResident(String uuid) {
        residents.add(uuid);
        townData.getYaml().set("residents", residents);
        townData.save();
    }

    public boolean kickResident(String uuid) {
        if(residents.contains(uuid)) {
            residents.remove(uuid);
            townData.getYaml().set("residents", residents);
            townData.save();
            return true;
        }
        return false;
    }

    public void removeTown() {
        Main.getTownDataManager().getDataMap().remove(uuid);
        townData.delete();
    }

    public String getRegisteredDate() {
        return registeredDate;
    }

    public int getLvl() {
        return lvl;
    }

    public int getRequiredExp() {
        return requiredExp;
    }

    public int getNowExperience() {
        return nowExperience;
    }

    public void addExp(int amount) {
        for(int x = 0; x < amount; x++) {
            nowExperience++;
            if(nowExperience > requiredExp) {
                int rest = nowExperience - requiredExp;
                lvlUp(rest);
            } else if(nowExperience == requiredExp) {
                lvlUp(0);
            }
        }
        townData.getYaml().set("nowExperience", nowExperience);
        townData.save();
    }

    private void lvlUp(int nowExperienceAmount) {
        lvl = lvl + 1;
        requiredExp = requiredExp*2;
        nowExperience = nowExperienceAmount;
        townData.getYaml().set("lvl", lvl);
        townData.getYaml().set("nowExperience", nowExperience);
        townData.getYaml().set("requiredExperience", requiredExp);
        townData.save();
        String[] title = plugin.getConfig().getString("messages.townLvlup").replace("%lvl%", String.valueOf(lvl)).split(";");
        getOnlinePlayers().forEach(townPlayer -> townPlayer.sendTitle(new ChatUtils(plugin).setColor(title[0]), new ChatUtils(plugin).setColor(title[1]), 20, 40, 20));
    }

    public List<String> getTownInfo(int index) {
        List<String> info = new ArrayList<>();
        for(String line : plugin.getConfig().getStringList("TownList.list")) {
            info.add(line
                    .replace("%index%", String.valueOf(index))
                    .replace("%town%", name)
                    .replace("%online%", String.valueOf(getOnlinePlayers().size()))
                    .replace("%residents%", String.valueOf(residents.size()))
                    .replace("%lvl%", String.valueOf(lvl))
                    .replace("%nowExp%", String.valueOf(nowExperience)));
        }
        return info;
    }

    public void rename(String newName) {
        this.townData.getYaml().set("townName", newName);
        this.townData.save();
        name = newName;
        Main.getTownDataManager().getDataMap().replace(uuid, townData);
    }
}
