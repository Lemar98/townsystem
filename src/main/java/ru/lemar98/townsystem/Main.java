package ru.lemar98.townsystem;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.commands.*;
import ru.lemar98.townsystem.database.TownDataManager;
import ru.lemar98.townsystem.invites.InvitesManager;
import ru.lemar98.townsystem.listeners.AsyncPlayerChat;

public class Main extends JavaPlugin {

    private static TownDataManager townDataManager;
    private static InvitesManager invitesManager;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        // ИНИЦИАЛИЗАЦИЯ МАП
        this.invitesManager = new InvitesManager(this);

        // ЗАГРУЗКА БАЗЫ
        this.townDataManager = new TownDataManager(this);
        this.townDataManager.loadAll();

        // РЕГИСТРАЦИЯ КОМАНД //
        getCommand("towncreate").setExecutor(new TownCreate(this));
        getCommand("townreload").setExecutor(new TownReload(this));
        getCommand("towninfo").setExecutor(new TownInfo(this));
        getCommand("towninvite").setExecutor(new TownInvite(this));
        getCommand("townkick").setExecutor(new TownKick(this));
        getCommand("townraise").setExecutor(new TownRaise(this));
        getCommand("townremove").setExecutor(new TownRemoveCommand(this));
        getCommand("towncleardata").setExecutor(new TownClearData(this));
        getCommand("townsetboard").setExecutor(new TownSetBoardCommand(this));
        getCommand("townlvlup").setExecutor(new TownLevelCommand(this));
        getCommand("townleave").setExecutor(new TownLeaveCommand(this));
        getCommand("townlist").setExecutor(new TownList(this));
        getCommand("townrename").setExecutor(new TownRenameCommand(this));
        getCommand("town").setExecutor(new TownCommand(this));

        // РЕГИСТРАЦИЯ ЛИСТЕНЕРОВ //
        Bukkit.getPluginManager().registerEvents(new AsyncPlayerChat(this), this);
    }

    @Override
    public void onDisable() {
        // СОХРАНЕНИЕ БАЗЫ //
        this.townDataManager.saveAll();
    }

    public static TownDataManager getTownDataManager() {
        return townDataManager;
    }

    public static InvitesManager getInvitesManager() {
        return invitesManager;
    }
}
