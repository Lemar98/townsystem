package ru.lemar98.townsystem.database;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.town.Town;

import java.io.File;

public class TownData {

    private YamlConfiguration yml;
    private File file;
    private String name;
    private JavaPlugin plugin;

    public TownData(String name, JavaPlugin plugin) {
        this.name = name;
        this.plugin = plugin;
        this.file = new File(plugin.getDataFolder()+"/Database/", name + ".yml");
        this.yml = YamlConfiguration.loadConfiguration(this.file);
        this.file.getParentFile().mkdirs();
    }

    public Town getTown() {
        return new Town(name, plugin);
    }

    public FileConfiguration getYaml() {
        return this.yml;
    }

    public void delete() {
        this.file.delete();
    }

    public void save() {
        try {
            this.yml.save(this.file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
