package ru.lemar98.townsystem.database;

import com.google.common.io.Files;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class TownDataManager {

    private Map<String, TownData> sfg = new ConcurrentHashMap<>();
    private JavaPlugin plugin;

    public TownDataManager(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    public TownData getTownData(String name) {
        if (this.sfg.get(convertTownNameToUuid(name)) == null) addTownData(UUID.randomUUID().toString());
        return this.sfg.get(convertTownNameToUuid(name));
    }

    public boolean townExist(String name) {
        return convertTownNameToUuid(name) != null ? true : false;
    }

    private String nameWithoutColorCodes(String name) {
        String newName;
        if(name.contains("&")) {
            newName = ChatColor.translateAlternateColorCodes('&', name);
        } else {
            newName = name;
        }
        return ChatColor.stripColor(newName);
    }

    public Map<String, TownData> getDataMap() {
        return this.sfg;
    }

    public void addTownData(String uuid) {
        TownData custom = new TownData(uuid, this.plugin);
        this.sfg.put(uuid, custom);
    }

    public void addTownData(String name, String ownerUuid, LocalDateTime date) {
        String uuid = UUID.randomUUID().toString();
        TownData custom = new TownData(uuid, this.plugin);
        custom.getYaml().set("townName", name);
        custom.getYaml().set("Mayor", ownerUuid);
        custom.getYaml().set("assistant", "!$none$!");
        custom.getYaml().set("townUuid", uuid);
        custom.getYaml().set("board", "отсутствует");
        custom.getYaml().set("lvl", plugin.getConfig().getInt("Settings.startLvl"));
        custom.getYaml().set("nowExperience", 0);
        custom.getYaml().set("requiredExperience", plugin.getConfig().getInt("Settings.startRequiredExp"));
        custom.getYaml().set("registeredDate", date.getDayOfMonth()+"."+date.getMonth().getValue()+"."+date.getYear());
        custom.getYaml().set("residents", Arrays.asList(ownerUuid));
        custom.save();
        this.sfg.put(uuid, custom);
    }

    public void save(String uuid) {
        if (this.sfg.get(uuid) == null) addTownData(uuid);
        try {
            this.sfg.get(uuid).save();
            this.sfg.remove(uuid);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadAll() {
        File databaseFolder = new File(plugin.getDataFolder()+"/Database/");
        if(databaseFolder == null || databaseFolder.listFiles() == null) return;
        for(File file : databaseFolder.listFiles()) {
            addTownData(Files.getNameWithoutExtension(file.getName()));
        }
    }

    public void saveAll() {
        for (String uuid : this.sfg.keySet()) {
            save(uuid);
        }
    }

    public boolean contains(String name) {
        if(convertTownNameToUuid(name) == null) return false;
        return this.sfg.containsKey(convertTownNameToUuid(name));
    }

    private String convertTownNameToUuid(String name) {
        for(TownData data : sfg.values()) {
            if(nameWithoutColorCodes(data.getTown().getName()).equals(name)) {
                return data.getTown().getUuid();
            }
        }
        return null;
    }
}