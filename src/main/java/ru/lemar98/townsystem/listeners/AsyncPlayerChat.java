package ru.lemar98.townsystem.listeners;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.utils.PlayerUtils;

public class AsyncPlayerChat implements Listener {

    private JavaPlugin plugin;

    public AsyncPlayerChat(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void chat(AsyncPlayerChatEvent event) {
        if(event.getFormat().contains("{twn}")) {
            String townName = PlayerUtils.hasTown(event.getPlayer().getUniqueId().toString())
                    ?  ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("Settings.chatPrefixFormat")
                    .replace("%town%", PlayerUtils.getTownByPlayerUuid(event.getPlayer().getUniqueId().toString()).getName()))
                    : "";
            event.setFormat(event.getFormat().replace("{twn}",  townName));
        }
    }
}
