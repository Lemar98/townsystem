package ru.lemar98.townsystem.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.database.TownData;
import ru.lemar98.townsystem.town.Town;
import ru.lemar98.townsystem.utils.ChatUtils;

import java.util.*;
import java.util.stream.Collectors;

public class TownList implements CommandExecutor {

    private ChatUtils chatUtils;
    private JavaPlugin plugin;

    public TownList(JavaPlugin plugin) {
        this.plugin = plugin;
        this.chatUtils = new ChatUtils(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(chatUtils.getMessage("playerOnly"));
            return true;
        }
        if(args.length > 1) {
            sender.sendMessage(chatUtils.getMessage("invalidArgs"));
            return true;
        }
        Player player = (Player)sender;
        if(!player.hasPermission("town.townlist")) {
            player.sendMessage(chatUtils.getMessage("noPerm"));
            return true;
        }
        if(args.length == 0) {
            sendPage(player, 1);
        } else {
            if (!isInt(args[0])) {
                player.sendMessage(chatUtils.getMessage("invalidArgs"));
                return true;
            }
            int enteredPage = Integer.valueOf(args[0]);
            sendPage(player, enteredPage);
            return true;
        }
        return true;
    }

    private void sendPage(Player player, int page) {
        int pages = 1;
        if(getSortedTowns().size() > 5) {
            if(getSortedTowns().size()%5 == 0) {
                pages = getSortedTowns().size()/5;
            } else if(getSortedTowns().size()%5 != 0) {
                pages =  getSortedTowns().size()/5 + 1;
            }
        }
        if(page > pages || page == 0) {
            player.sendMessage(chatUtils.getMessage("noPage"));
            return;
        }
        sendFooter(player);
        for(int x = 0; x < getSortedTowns().size(); x++) {
            if(x != 0 && x != 1 && x%5 == 0) {
                break;
            }
            if(page != 1 && x < 5) {
                x = (page * 5) - 5;
            }
            for(String infoLine : getSortedTowns().get(x).getTownInfo(x+1)) {
                player.sendMessage(chatUtils.setColor(infoLine));
            }
        }
        sendBottom(player, page, pages);
    }

    private void sendFooter(Player player) {
        for(String footerLine : plugin.getConfig().getStringList("TownList.footer")) {
            player.sendMessage(chatUtils.setColor(footerLine));
        }
    }

    private void sendBottom(Player player, int page, int pages) {
        for(String bottomLine : plugin.getConfig().getStringList("TownList.bottom")) {
            player.sendMessage(chatUtils.setColor(bottomLine)
                    .replace("%nextPage%", String.valueOf(page+1))
                    .replace("%page%", String.valueOf(page))
                    .replace("%pages%", String.valueOf(pages)));
        }
    }

    private List<Town> getSortedTowns() {
        List<Town> townList = new ArrayList<>();
        for(TownData data : Main.getTownDataManager().getDataMap().values()) {
            townList.add(data.getTown());
        }
        List sorted = townList.stream().sorted(Comparator.comparingInt(Town::getLvl).thenComparingInt(Town::getNowExperience)).collect(Collectors.toList());
        Collections.reverse(sorted);
        townList = sorted;
        return townList;
    }

    private boolean isInt(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
}
