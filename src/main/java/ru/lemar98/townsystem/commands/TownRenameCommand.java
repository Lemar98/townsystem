package ru.lemar98.townsystem.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.town.Town;
import ru.lemar98.townsystem.utils.ChatUtils;
import ru.lemar98.townsystem.utils.PlayerUtils;

public class TownRenameCommand implements CommandExecutor {

    private ChatUtils chatUtils;
    private JavaPlugin plugin;

    public TownRenameCommand(JavaPlugin plugin) {
        this.chatUtils = new ChatUtils(plugin);
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(chatUtils.getMessage("playerOnly"));
            return true;
        }
        if(args.length != 1) {
            sender.sendMessage(chatUtils.getMessage("invalidArgs"));
            return true;
        }
        Player player = (Player)sender;
        if(!PlayerUtils.hasTown(player.getUniqueId().toString())) {
            player.sendMessage(chatUtils.getMessage("notTown"));
            return true;
        }
        String newName = args[0];
        if(Main.getTownDataManager().townExist(newName)) {
           player.sendMessage(chatUtils.getMessage("townExist"));
           return true;
        }
        String[] split = plugin.getConfig().getString("Settings.itemForRename").split(";");
        ItemStack itemStack = new ItemStack(Material.getMaterial(split[0].toUpperCase()), Integer.valueOf(split[1]));
        if(!player.getInventory().contains(itemStack)) {
            player.sendMessage(chatUtils.getMessage("renameItemNeed"));
            return true;
        }
        if(newName.contains("&") && !sender.hasPermission("town.useChatColor")) {
            sender.sendMessage(chatUtils.getMessage("noPermChatColor"));
            return true;
        }
        for(String color : plugin.getConfig().getStringList("NotUseColors")) {
            if(newName.contains(color)) {
                player.sendMessage(chatUtils.getMessage("useProhibitColor"));
                return true;
            }
        }
        Town town = PlayerUtils.getTownByPlayerUuid(player.getUniqueId().toString());
        town.rename(newName);
        player.getInventory().removeItem(itemStack);
        town.getOnlinePlayers().forEach(townPlayer -> townPlayer.sendMessage(chatUtils.getMessage("renamed")
                               .replace("%newName%", chatUtils.setColor(newName))));
        return true;
    }
}
