package ru.lemar98.townsystem.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.town.Town;
import ru.lemar98.townsystem.utils.ChatUtils;
import ru.lemar98.townsystem.utils.PlayerUtils;

public class TownLeaveCommand implements CommandExecutor {

    private ChatUtils chatUtils;

    public TownLeaveCommand(JavaPlugin plugin) {
        this.chatUtils = new ChatUtils(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(chatUtils.getMessage("playerOnly"));
            return true;
        }
        if(args.length != 0) {
            sender.sendMessage(chatUtils.getMessage("invalidArgs"));
            return true;
        }
        Player player = (Player)sender;
        if(!PlayerUtils.hasTown(player.getUniqueId().toString())) {
            player.sendMessage(chatUtils.getMessage("notTown"));
            return true;
        }
        Town town = PlayerUtils.getTownByPlayerUuid(player.getUniqueId().toString());
        if(town.isMayor(player.getUniqueId().toString())) {
            if(!town.hasAssistant()) {
                town.removeTown();
                player.sendMessage(chatUtils.getMessage("youAreTownLeave"));
                town.getOnlinePlayers().forEach(townPlayer -> townPlayer.sendMessage(chatUtils.getMessage("mayorLeave")));
                return true;
            }
            player.sendMessage(chatUtils.getMessage("youAreTownLeave"));
            town.getOnlinePlayers().forEach(townPlayer -> townPlayer.sendMessage(chatUtils.getMessage("mayorLeave2").replace("%assistant%", PlayerUtils.convertUuidToName(town.getAssistant()))));
            town.setMayor(town.getAssistant());
            town.kickResident(player.getUniqueId().toString());
            town.setAssistant("!$none$!");
            return true;
        } else if(town.isAssistant(player.getUniqueId().toString())) {
            town.setAssistant("!$none$!");
            town.kickResident(player.getUniqueId().toString());
            player.sendMessage(chatUtils.getMessage("youAreTownLeave"));
            town.getOnlinePlayers().forEach(townPlayer -> townPlayer.sendMessage(chatUtils.getMessage("targetIsLeave").replace("%target%", player.getName())));
            return true;
        } else {
            town.kickResident(player.getUniqueId().toString());
            player.sendMessage(chatUtils.getMessage("youAreTownLeave"));
            town.getOnlinePlayers().forEach(townPlayer -> townPlayer.sendMessage(chatUtils.getMessage("targetIsLeave").replace("%target%", player.getName())));
            return true;
        }
    }
}
