package ru.lemar98.townsystem.commands;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.town.Town;
import ru.lemar98.townsystem.utils.ChatUtils;
import ru.lemar98.townsystem.utils.PlayerUtils;

public class TownLevelCommand implements CommandExecutor {

    private ChatUtils chatUtils;
    private JavaPlugin plugin;

    public TownLevelCommand(JavaPlugin plugin) {
        this.chatUtils = new ChatUtils(plugin);
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(chatUtils.getMessage("playerOnly"));
            return true;
        }
        if(args.length != 0) {
            sender.sendMessage(chatUtils.getMessage("invalidArgs"));
            return true;
        }
        Player player = (Player)sender;
        if(!PlayerUtils.hasTown(player.getUniqueId().toString())) {
            player.sendMessage(chatUtils.getMessage("notTown"));
            return true;
        }
        String needMaterial = plugin.getConfig().getString("Settings.itemForLvlUp").toUpperCase();
        ItemStack inHand = player.getInventory().getItemInMainHand();
        if(inHand == null || inHand.getType() != Material.getMaterial(needMaterial)) {
            player.sendMessage(chatUtils.getMessage("notFoundItemInHand"));
            return true;
        }
        Town town = PlayerUtils.getTownByPlayerUuid(player.getUniqueId().toString());
        int amount = inHand.getAmount();
        player.getInventory().getItemInMainHand().setAmount(amount - amount);
        town.addExp(amount);
        town.getOnlinePlayers().forEach(townPlayer -> townPlayer.sendMessage(chatUtils.getMessage("addedExp")
                .replace("%resident%", player.getName())
                .replace("%amount%", String.valueOf(amount))));
        return true;
    }
}
