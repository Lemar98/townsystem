package ru.lemar98.townsystem.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.town.Town;
import ru.lemar98.townsystem.utils.ChatUtils;
import ru.lemar98.townsystem.utils.PlayerUtils;

public class TownRaise implements CommandExecutor {

    private ChatUtils chatUtils;

    public TownRaise(JavaPlugin plugin) {
        this.chatUtils = new ChatUtils(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(chatUtils.getMessage("playerOnly"));
            return true;
        }
        if(args.length != 1) {
            sender.sendMessage(chatUtils.getMessage("invalidArgs"));
            return true;
        }
        Player player = (Player)sender;
        Player target = Bukkit.getPlayerExact(args[0]);
        if(target == null) {
            player.sendMessage(chatUtils.getMessage("playerNotFound"));
            return true;
        }
        if(!PlayerUtils.hasTown(player.getUniqueId().toString())) {
            player.sendMessage(chatUtils.getMessage("notTown"));
            return true;

        }
        if(!PlayerUtils.hasTown(target.getUniqueId().toString())) {
            player.sendMessage(chatUtils.getMessage("residentIsNotInYourTown"));
            return true;
        }
        if(target == player) {
            player.sendMessage(chatUtils.getMessage("assistantYourself"));
            return true;
        }
        Town town = PlayerUtils.getTownByPlayerUuid(player.getUniqueId().toString());
        if(town.isAssistant(target.getUniqueId().toString())) {
            player.sendMessage(chatUtils.getMessage("nowAssistant"));
            return true;
        }
        town.setAssistant(target.getUniqueId().toString());
        player.sendMessage(chatUtils.getMessage("setAssistant"));
        target.sendMessage(chatUtils.getMessage("youSetAssistant"));
        town.getOnlinePlayers().stream()
                .filter(townPlayer -> townPlayer != player && townPlayer != target)
                .forEach(townPlayer -> townPlayer.sendMessage(chatUtils.getMessage("townSetAssistant")
                .replace("%mayor%", player.getName())
                .replace("%assistant%", target.getName())));
        return true;
    }
}
