package ru.lemar98.townsystem.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.utils.ChatUtils;
import ru.lemar98.townsystem.utils.PlayerUtils;

import java.time.LocalDateTime;

public class TownCreate implements CommandExecutor {

    private JavaPlugin plugin;
    private ChatUtils chatUtils;

    public TownCreate(JavaPlugin plugin) {
        this.plugin = plugin;
        this.chatUtils = new ChatUtils(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(chatUtils.getMessage("playerOnly"));
            return true;
        }
        if(args.length != 1) {
            sender.sendMessage(chatUtils.getMessage("invalidArgs"));
            return true;
        }
        String townName = args[0];
        Player player = (Player)sender;
        if(PlayerUtils.hasTown(player.getUniqueId().toString())) {
            sender.sendMessage(chatUtils.getMessage("hasTown"));
            return true;
        }
        if(Main.getTownDataManager().getDataMap().containsKey(townName)) {
            sender.sendMessage(chatUtils.getMessage("townExist"));
            return true;
        }
        if(townName.contains("&") && !sender.hasPermission("town.useChatColor")) {
            sender.sendMessage(chatUtils.getMessage("noPermChatColor"));
            return true;
        }
        for(String color : plugin.getConfig().getStringList("NotUseColors")) {
            if(townName.contains(color)) {
                player.sendMessage(chatUtils.getMessage("useProhibitColor"));
                return true;
            }
        }
        Main.getTownDataManager().addTownData(townName, player.getUniqueId().toString(), LocalDateTime.now());
        player.sendMessage(chatUtils.getMessage("townCreateSuccess").replace("%town%", chatUtils.setColor(townName)));
        return true;
    }
}
