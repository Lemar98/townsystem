package ru.lemar98.townsystem.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.database.TownData;
import ru.lemar98.townsystem.town.Town;
import ru.lemar98.townsystem.utils.ChatUtils;

public class TownClearData implements CommandExecutor {

    public ChatUtils chatUtils;

    public TownClearData(JavaPlugin plugin) {
        this.chatUtils = new ChatUtils(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!sender.isOp()) {
            sender.sendMessage(chatUtils.getMessage("noPerm"));
            return true;
        }
        if(args.length != 0) {
            sender.sendMessage(chatUtils.getMessage("invalidArgs"));
            return true;
        }
        for(TownData townData : Main.getTownDataManager().getDataMap().values()) {
            Town town = townData.getTown();
            town.removeTown();
            System.out.println(ChatColor.GOLD + "Town " + ChatColor.RED + town.getName() + ChatColor.GOLD + " has been removed!");
        }
        sender.sendMessage(chatUtils.getMessage("townsDeleted"));
        return true;
    }
}
