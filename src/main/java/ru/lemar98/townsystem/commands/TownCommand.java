package ru.lemar98.townsystem.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.utils.ChatUtils;

public class TownCommand implements CommandExecutor {

    private ChatUtils chatUtils;
    private JavaPlugin plugin;

    public TownCommand(JavaPlugin plugin) {
        this.chatUtils = new ChatUtils(plugin);
        this.plugin = plugin;
    }

    //TODO: ПОФИКСИТЬ КОСТЫЛИ

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(chatUtils.getMessage("playerOnly"));
            return true;
        }
        Player player = (Player) sender;
        if (args.length == 0) {
            Bukkit.dispatchCommand(player, "towninfo");
            return true;
        }
        switch (args.length) {
            case 1: {
                if (Main.getTownDataManager().contains(args[0])) {
                    Bukkit.dispatchCommand(player, "towninfo " + args[0]);
                    return true;
                }
                switch (args[0]) {
                    case "cleardata": {
                        Bukkit.dispatchCommand(player, "towncleardata");
                        return true;
                    }
                    case "leave": {
                        Bukkit.dispatchCommand(player, "townleave");
                        return true;
                    }
                    case "lvlup": {
                        Bukkit.dispatchCommand(player, "townlvlup");
                        return true;
                    }
                    case "list": {
                        Bukkit.dispatchCommand(player, "townlist");
                        return true;
                    }
                    case "reload": {
                        Bukkit.dispatchCommand(player, "townreload");
                        return true;
                    }
                    case "delete": {
                        Bukkit.dispatchCommand(player, "townremove");
                        return true;
                    }
                    case "help": {
                        for(String line : plugin.getConfig().getStringList("HelpList")) {
                            player.sendMessage(chatUtils.setColor(line));
                        }
                        return true;
                    }
                    default: return true;
                }
            }
            case 2: {
                try {
                    switch (args[0]) {
                        case "create": {
                            Bukkit.dispatchCommand(player, "towncreate " + args[1]);
                            return true;
                        }
                        case "invite": {
                            Bukkit.dispatchCommand(player, "towninvite " + args[1]);
                            return true;
                        }
                        case "kick": {
                            Bukkit.dispatchCommand(player, "townkick " + args[1]);
                            return true;
                        }
                        case "list": {
                            Bukkit.dispatchCommand(player, "townlist " + args[1]);
                            return true;
                        }
                        case "rank": {
                            Bukkit.dispatchCommand(player, "townraise " + args[1]);
                            return true;
                        }
                        case "setboard": {
                            Bukkit.dispatchCommand(player, "townsetboard " + String.join(",", args));
                            return true;
                        }
                        case "delete": {
                            Bukkit.dispatchCommand(player, "townremove " + args[1]);
                            return true;
                        }
                        case "rename": {
                            Bukkit.dispatchCommand(player, "townrename " + args[1]);
                            return true;
                        }
                        default:
                            player.sendMessage(chatUtils.getMessage("invalidArgs"));
                    }
                } catch (ArrayIndexOutOfBoundsException ex) {
                    return true;
                }
            }
            default:

                return true;
        }
    }
}
