package ru.lemar98.townsystem.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.town.Town;
import ru.lemar98.townsystem.utils.ChatUtils;
import ru.lemar98.townsystem.utils.PlayerUtils;

public class TownKick implements CommandExecutor {

    private ChatUtils chatUtils;

    public TownKick(JavaPlugin plugin) {
        this.chatUtils = new ChatUtils(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(chatUtils.getMessage("playerOnly"));
            return true;
        }
        if(args.length != 1) {
            sender.sendMessage(chatUtils.getMessage("invalidArgs"));
            return true;
        }
        Player player = (Player) sender;
        if(!PlayerUtils.hasTown(player.getUniqueId().toString())) {
            player.sendMessage(chatUtils.getMessage("youHasNoTown"));
            return true;
        }
        Town town = PlayerUtils.getTownByPlayerUuid(player.getUniqueId().toString());
        if(!town.isMayor(player.getUniqueId().toString()) && !town.isAssistant(player.getUniqueId().toString())) {
            player.sendMessage(chatUtils.getMessage("noPerm"));
            return true;
        }
        if(args[0].equals(player.getName())) {
            player.sendMessage(chatUtils.getMessage("kickYourself"));
            return true;
        }
        if(!town.getResidents().contains(PlayerUtils.convertNameToUuid(args[0]))) {
            player.sendMessage(chatUtils.getMessage("residentIsNotInYourTown"));
            return true;
        }
        town.kickResident(PlayerUtils.convertNameToUuid(args[0]));
        player.sendMessage(chatUtils.getMessage("kickSuccessful").replace("%target%", args[0]));
        town.getOnlinePlayers().stream()
                .filter(townPlayer -> townPlayer != player)
                .forEach(townPlayer -> townPlayer.sendMessage(chatUtils.getMessage("broadcastKick")
                        .replace("%target%", args[0])));
        if(Bukkit.getPlayer(args[0]) != null) {
            Bukkit.getPlayer(args[0]).sendMessage(chatUtils.getMessage("kickYou"));
        }
        return true;
    }
}
