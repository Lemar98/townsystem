package ru.lemar98.townsystem.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.utils.ChatUtils;

public class TownReload implements CommandExecutor {

    private ChatUtils chatUtils;
    private JavaPlugin plugin;

    public TownReload(JavaPlugin plugin) {
        this.plugin = plugin;
        this.chatUtils = new ChatUtils(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!sender.isOp()) {
            sender.sendMessage(chatUtils.getMessage("noPerm"));
            return true;
        }
        if(args.length != 0) {
            sender.sendMessage(chatUtils.getMessage("invalidArgs"));
            return true;
        }
        plugin.reloadConfig();
        sender.sendMessage(chatUtils.getMessage("reloadSuccess"));
        return true;
    }
}
