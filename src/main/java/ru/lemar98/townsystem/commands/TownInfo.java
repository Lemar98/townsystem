package ru.lemar98.townsystem.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.town.Town;
import ru.lemar98.townsystem.utils.ChatUtils;
import ru.lemar98.townsystem.utils.PlayerUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TownInfo implements CommandExecutor {

    private JavaPlugin plugin;
    private ChatUtils chatUtils;

    public TownInfo(JavaPlugin plugin) {
        this.plugin = plugin;
        this.chatUtils = new ChatUtils(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length == 0) {
            if(!(sender instanceof Player)) {
                sender.sendMessage(chatUtils.getMessage("playerOnly"));
                return true;
            } else {
                Player player = (Player)sender;
                if(!PlayerUtils.hasTown(player.getUniqueId().toString())) {
                    player.sendMessage(chatUtils.getMessage("notTown"));
                    return true;
                }
                Town town = PlayerUtils.getTownByPlayerUuid(player.getUniqueId().toString());
                for(String info : plugin.getConfig().getStringList("TownInformation")) {
                    player.sendMessage(chatUtils.setColor(info)
                            .replace("%town%", chatUtils.setColor(town.getName()))
                            .replace("%residents%", getResidentsNames(town))
                            .replace("%mayor%", PlayerUtils.convertUuidToName(town.getMayor()))
                            .replace("%assistant%", PlayerUtils.convertUuidToName(town.getAssistant()))
                            .replace("%count%", String.valueOf(town.getResidents().size()))
                            .replace("%board%", town.getBoard())
                            .replace("%date%", town.getRegisteredDate())
                            .replace("%online%", String.valueOf(town.getOnlinePlayers().size()))
                            .replace("%lvl%", String.valueOf(town.getLvl()))
                            .replace("%nowExp%", String.valueOf(town.getNowExperience()))
                            .replace("%required%", String.valueOf(town.getRequiredExp())));
                }
                return true;
            }
        } else if(args.length == 1) {
            if(!Main.getTownDataManager().townExist(args[0])) {
                sender.sendMessage(chatUtils.getMessage("invalidTown"));
                return true;
            }
            Town town = Main.getTownDataManager().getTownData(args[0]).getTown();
            if(sender.hasPermission("town.getInformation")) {
                for(String info : plugin.getConfig().getStringList("TownInformation")) {
                    sender.sendMessage(chatUtils.setColor(info)
                            .replace("%town%", chatUtils.setColor(town.getName()))
                            .replace("%residents%", getResidentsNames(town))
                            .replace("%mayor%", PlayerUtils.convertUuidToName(town.getMayor()))
                            .replace("%assistant%", PlayerUtils.convertUuidToName(town.getAssistant()))
                            .replace("%count%", String.valueOf(town.getResidents().size()))
                            .replace("%board%", town.getBoard())
                            .replace("%date%", town.getRegisteredDate())
                            .replace("%online%", String.valueOf(town.getOnlinePlayers().size()))
                            .replace("%lvl%", String.valueOf(town.getLvl()))
                            .replace("%nowExp%", String.valueOf(town.getNowExperience()))
                            .replace("%required%", String.valueOf(town.getRequiredExp())));
                }
                return true;
            } else {
                sender.sendMessage(chatUtils.getMessage("noPerm"));
                return true;
            }
        } else if(args.length > 1) {
            sender.sendMessage(chatUtils.getMessage("invalidArgs"));
            return true;
        }
        return true;
    }

    private String getResidentsNames(Town town) {
        List<String> residentsName = new ArrayList<>();
        for(String uuid : town.getResidents()) {
            residentsName.add(PlayerUtils.convertUuidToName(uuid));
        }
        return String.join(", ", residentsName);
    }
}
