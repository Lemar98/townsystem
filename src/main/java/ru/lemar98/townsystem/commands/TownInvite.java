package ru.lemar98.townsystem.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.town.Town;
import ru.lemar98.townsystem.utils.ChatUtils;
import ru.lemar98.townsystem.utils.PlayerUtils;

public class TownInvite implements CommandExecutor {

    private ChatUtils chatUtils;

    public TownInvite(JavaPlugin plugin) {
        this.chatUtils = new ChatUtils(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage(chatUtils.getMessage("playerOnly"));
            return true;
        }
        if(args.length != 1) {
            sender.sendMessage(chatUtils.getMessage("invalidArgs"));
            return true;
        }
        Player player = (Player) sender;
        if(!args[0].equals("agree") && !args[0].equals("disagree")) {
            if(!PlayerUtils.hasTown(player.getUniqueId().toString())) {
                player.sendMessage(chatUtils.getMessage("youHasNoTown"));
                return true;
            }
            Town town = PlayerUtils.getTownByPlayerUuid(player.getUniqueId().toString());
            if(!town.isAssistant(player.getUniqueId().toString()) && !town.isMayor(player.getUniqueId().toString())) {
                player.sendMessage(chatUtils.getMessage("noPerm"));
                return true;
            }
            Player reciever = Bukkit.getPlayer(args[0]);
            if(reciever == null) {
                player.sendMessage(chatUtils.getMessage("playerNotFound"));
                return true;
            }
            if(reciever.getName().equals(player.getName())) {
                player.sendMessage(chatUtils.getMessage("sendYourself"));
                return true;
            }
            if(Main.getInvitesManager().hasInvite(reciever)) {
                player.sendMessage(chatUtils.getMessage("targetHasInvite"));
                return true;
            }
            if(PlayerUtils.hasTown(reciever.getUniqueId().toString())) {
                player.sendMessage(chatUtils.getMessage("targetHasTown"));
                return true;
            }
            Main.getInvitesManager().sendInvite(player, reciever, town);
            return true;
        } else {
            if(args[0].equals("agree")) {
                if(!Main.getInvitesManager().hasInvite(player)) {
                    player.sendMessage(chatUtils.getMessage("noInvites"));
                    return true;
                }
                Main.getInvitesManager().acceptInvite(player);
                return true;
            }
            if(args[0].equals("disagree")) {
                if(!Main.getInvitesManager().hasInvite(player)) {
                    player.sendMessage(chatUtils.getMessage("noInvites"));
                    return true;
                }
                Main.getInvitesManager().denyInvite(player);
                return true;
            }
            return true;
        }
    }
}
