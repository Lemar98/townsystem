package ru.lemar98.townsystem.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.town.Town;
import ru.lemar98.townsystem.utils.ChatUtils;
import ru.lemar98.townsystem.utils.PlayerUtils;

public class TownRemoveCommand implements CommandExecutor {

    private ChatUtils chatUtils;

    public TownRemoveCommand(JavaPlugin plugin) {
        this.chatUtils = new ChatUtils(plugin);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender == Bukkit.getConsoleSender()) {
            if(args.length != 1) {
                sender.sendMessage(chatUtils.getMessage("invalidArgs"));
                return true;
            }
            if(!Main.getTownDataManager().townExist(args[0])) {
                sender.sendMessage(chatUtils.getMessage("invalidTown"));
                return true;
            }
            Town town = Main.getTownDataManager().getTownData(args[0]).getTown();
            town.getOnlinePlayers().forEach(townPlayer -> townPlayer.sendMessage(chatUtils.getMessage("ourTownDeleteByAdmin")));
            sender.sendMessage(chatUtils.getMessage("townDelete"));
            town.removeTown();
            return true;
        } else {
            Player player = (Player)sender;
            if(args.length == 0) {
                if (!PlayerUtils.hasTown(player.getUniqueId().toString())) {
                    player.sendMessage(chatUtils.getMessage("notTown"));
                    return true;
                }
                Town town = PlayerUtils.getTownByPlayerUuid(player.getUniqueId().toString());
                if (!town.isMayor(player.getUniqueId().toString())) {
                    player.sendMessage(chatUtils.getMessage("noPerm"));
                    return true;
                }
                town.getOnlinePlayers().stream()
                        .filter(townPlayer -> townPlayer != player)
                        .forEach(townPlayer -> townPlayer.sendMessage(chatUtils.getMessage("ourTownDelete")));
                player.sendMessage(chatUtils.getMessage("townDelete"));
                town.removeTown();
                return true;
            } else if(args.length == 1) {
                if(!player.isOp()) {
                    player.sendMessage(chatUtils.getMessage("noPerm"));
                    return true;
                }
                if(!Main.getTownDataManager().townExist(args[0])) {
                    sender.sendMessage(chatUtils.getMessage("invalidTown"));
                    return true;
                }
                Town town = Main.getTownDataManager().getTownData(args[0]).getTown();
                town.getOnlinePlayers().forEach(townPlayer -> townPlayer.sendMessage(chatUtils.getMessage("ourTownDeleteByAdmin")));
                player.sendMessage(chatUtils.getMessage("townDelete"));
                town.removeTown();
                return true;
            } else {
                player.sendMessage(chatUtils.getMessage("invalidArgs"));
                return true;
            }
        }
    }
}
