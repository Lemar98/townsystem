package ru.lemar98.townsystem.invites;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.lemar98.townsystem.town.Town;
import ru.lemar98.townsystem.utils.ChatUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class InvitesManager {

    private Map<String, Invite> invites;
    private JavaPlugin plugin;
    private ChatUtils chatUtils;

    public InvitesManager(JavaPlugin plugin) {
        this.invites = new HashMap<>();
        this.plugin = plugin;
        this.chatUtils = new ChatUtils(plugin);
    }

    public void removeInvite(Invite invite) {
        if(invites.containsValue(invite)) {
            invites.remove(invite.getReciever());
        }
    }

    public void sendInvite(Player sender, Player target, Town town) {
        Invite invite = new Invite(plugin, sender.getUniqueId().toString(), target.getUniqueId().toString(), town);
        invite.runTaskLater(plugin, 20L*plugin.getConfig().getLong("Settings.inviteLongTime"));
        invites.put(target.getUniqueId().toString(), invite);
        TextComponent agree = new TextComponent(" [Принять] ");
        TextComponent disagree = new TextComponent("[Отклонить]");
        agree.setColor(ChatColor.GREEN);
        agree.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/towninvite agree"));
        agree.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(chatUtils.setColor("&aНажмите, чтобы принять")).create()));
        disagree.setColor(ChatColor.RED);
        disagree.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/towninvite disagree"));
        disagree.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(chatUtils.setColor("&cНажмите, чтобы отклонить")).create()));
        TextComponent cfgText = new TextComponent(chatUtils.getMessage("inviteAgree")
                .replace("%town%", town.getName())
                .replace("%sender%", sender.getName()));
        target.spigot().sendMessage(cfgText, agree, disagree);
        sender.sendMessage(chatUtils.getMessage("sendInvite").replace("%target%", target.getName()));
    }

    public void acceptInvite(Player target) {
        Invite invite = invites.get(target.getUniqueId().toString());
        invite.cancel();
        invite.getTown().addResident(target.getUniqueId().toString());
        Bukkit.getPlayer(UUID.fromString(invite.getSender())).sendMessage(chatUtils.getMessage("targetAccept").replace("%target%", target.getName()));
        target.sendMessage(chatUtils.getMessage("youAreJoin").replace("%town%", invite.getTown().getName()));
        invite.getTown().getOnlinePlayers().stream()
                .filter(player -> player != target)
                .forEach(player -> player.sendMessage(chatUtils.getMessage("hasJoinedTown").replace("%target%", target.getName())));
        invites.remove(invite.getReciever());

    }

    public void denyInvite(Player target) {
        Invite invite = invites.get(target.getUniqueId().toString());
        invite.cancel();
        Bukkit.getPlayer(UUID.fromString(invite.getSender())).sendMessage(chatUtils.getMessage("inviteDeny").replace("%reciever%", target.getName()));
        target.sendMessage(chatUtils.getMessage("youAreDeny"));
        invites.remove(target);
    }

    public boolean hasInvite(Player target) {
        return invites.containsKey(target.getUniqueId().toString());
    }
}
