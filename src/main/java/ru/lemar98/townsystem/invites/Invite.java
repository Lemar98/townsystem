package ru.lemar98.townsystem.invites;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.town.Town;
import ru.lemar98.townsystem.utils.ChatUtils;

import java.util.UUID;

public class Invite extends BukkitRunnable {

    private String sender;
    private String reciever;
    private Town town;
    private ChatUtils chatUtils;

    public Invite(JavaPlugin plugin, String sender, String reciever, Town town) {
        this.sender = sender;
        this.reciever = reciever;
        this.town = town;
        this.chatUtils = new ChatUtils(plugin);
    }

    @Override
    public void run() {
        Main.getInvitesManager().removeInvite(this);
        cancel();
        if(Bukkit.getPlayer(UUID.fromString(reciever)) != null) {
            Bukkit.getPlayer(UUID.fromString(reciever)).sendMessage(chatUtils.getMessage("inviteTimeLeftReciever"));
        }
        if(Bukkit.getPlayer(UUID.fromString(sender)) != null) {
            Bukkit.getPlayer(UUID.fromString(sender)).sendMessage(chatUtils.getMessage("inviteTimeLeftSender").replace("%reciever%", Bukkit.getPlayer(UUID.fromString(reciever)).getName()));
        }
    }

    public String getSender() {
        return sender;
    }

    public String getReciever() {
        return reciever;
    }

    public Town getTown() {
        return town;
    }

}
