package ru.lemar98.townsystem.utils;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class ChatUtils {

    private JavaPlugin plugin;

    public ChatUtils(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    public String getMessage(String message) {
        return setColor(this.plugin.getConfig().getString("messages."+message));
    }

    public String setColor(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }
}
