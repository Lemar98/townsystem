package ru.lemar98.townsystem.utils;

import org.bukkit.Bukkit;
import ru.lemar98.townsystem.Main;
import ru.lemar98.townsystem.database.TownData;
import ru.lemar98.townsystem.town.Town;

import java.util.UUID;

public class PlayerUtils {

    public static boolean hasTown(String uuid) {
        for(TownData townData : Main.getTownDataManager().getDataMap().values()) {
            if(townData.getTown().getResidents().contains(uuid)) return true;
        }
        return false;
    }

    public static Town getTownByPlayerUuid(String playerUuid) {
        for(TownData townData : Main.getTownDataManager().getDataMap().values()) {
            if(townData.getTown().getResidents().contains(playerUuid)) {
                return townData.getTown();
            }
        }
        return null;
    }

    public static String convertUuidToName(String uuid) {
        if(uuid.equals("!$none$!") || uuid.equals("нет")) {
            return "нет";
        }
        if(Bukkit.getPlayer(UUID.fromString(uuid)) != null) {
            return Bukkit.getPlayer(UUID.fromString(uuid)).getName();
        } else {
            return Bukkit.getOfflinePlayer(UUID.fromString(uuid)).getName();
        }
    }

    public static String convertNameToUuid(String name) {
        if(Bukkit.getPlayer(name) != null) {
            return Bukkit.getPlayer(name).getUniqueId().toString();
        } else {
            return Bukkit.getOfflinePlayer(name).getUniqueId().toString();
        }
    }
}
